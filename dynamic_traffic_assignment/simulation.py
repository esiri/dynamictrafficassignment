import numpy as np
import networkx as nx
import itertools
import warnings

import dynamic_traffic_assignment.network.fundamental_diagrams as fundamental_diagrams
import dynamic_traffic_assignment.network.junction_solvers as junction_solvers
import dynamic_traffic_assignment.utils.custom_exceptions as custom_exceptions
import dynamic_traffic_assignment.utils.utils as utils
import dynamic_traffic_assignment.users.veh_types as veh_types
import dynamic_traffic_assignment.users.user_behaviour as behaviours

from dynamic_traffic_assignment.network.network import Edge, Node, Path
from dynamic_traffic_assignment.utils.utils import show_obj_attributes

import dynamic_traffic_assignment.utils.decorators as dec


from dynamic_traffic_assignment.users.user_class import User


class Simulation:
    """"""

    __ALLOWED_PARAM_VALUES = {
        'sim_horizon': [0, np.inf],
        'fundamental_diagram': ['triangular', 'greenshields'],
        'junction_solver': ['soft_priority_rs', 'priority_rs'],
        'chosen_dx': [0, np.inf],
        'chosen_dt': [0, np.inf],
        'priority_tolerance': [0, 0.05],
        'distribution_tolerance': [0, 0.05],
        'demand_priority': [0, 1]
    }

    __REQUIRED_PARAM = {
        'sim_horizon',
        'fundamental_diagram',
        'junction_solver',
        'chosen_dx',
        'priority_tolerance',
        'distribution_tolerance',
        'demand_priority'
    }

    __OPTIONAL_PARAM = {
        'chosen_dt'
    }

    __TYPE_PARAM = {
        'sim_horizon': 'numeric',
        'fundamental_diagram': 'set',
        'junction_solver': 'set',
        'chosen_dx': 'numeric',
        'chosen_dt': 'numeric',  # optional
        'priority_tolerance': 'numeric',
        'distribution_tolerance': 'numeric',
        'demand_priority': 'dictionary'
    }

    __DICT_for_COMPOSITION = {
        # Fundamental diagrams
        'triangular': fundamental_diagrams.Triangular,
        'greenshields': fundamental_diagrams.Greenshields,

        # Junction Solvers
        'soft_priority_rs': junction_solvers.SoftPriority,
        'priority_rs': junction_solvers.Priority,

        # Vehicle types classes
        'car': veh_types.Car,
        'truck': veh_types.Truck,
        'car_agatha': veh_types.Car_Agatha,
        'bike_agatha': veh_types.Bike_Agatha,

        # User behaviours
        'logit_shortest': behaviours.Logit_shortest,
        'fixed_paths': behaviours.Fixed_paths
    }

    def __init__(self, config: dict, network):
        """
        Constructor function: triggers all the functions required for the init pipeline.
            * __set_sim_attribute()
            * __set_junction_solver_mode()
            * __set_veh_fd_parameters()
            * __set_user_classes()
            * __set_network()
            * __set_link_strategies_to_distribution_tensors()
        """

        # INIT PIPELINE
        self.__set_sim_attribute(config)        
        self.__set_veh_fd_parameters()
        paths = self.__set_user_classes(config, network)
        self.__set_network(network, paths,config)
        self.__link_strategies_to_distribution_tensors()

    def run(self):
        """
        Run the simulation
        """
        # PRE-RUN PIPELINE
        self.__mod_on_the_fly()
        self.__cfl()
        self.__time_discretization()
        self.__project_user_demand_supply_over_time()
        self.__load_initial_conditions()
        self.__init_demands_supplies()
        self.__init_edge_behavioural_determinants()

        # time loop
        for time_instant in range(self.NUM_INSTANTS - 1):
            self.__update_path_behavioural_determinants(time_step=time_instant)
            self.__compute_route_choices(time_instant)
            self.__solve_junctions(time_instant)
            self.__solve_edges(time_instant)
    
    @dec.check_all_required_parameters(__REQUIRED_PARAM)  
    def __set_sim_attribute(self,config:dict):

        # required parameters
        self.SIM_HORIZON = config['sim_horizon']
        self.CHOSEN_DX = config['chosen_dx']
        self.FUNDAMENTAL_DIAGRAM = self.__DICT_for_COMPOSITION[config['fundamental_diagram']]()
        self.JUNCTION_SOLVER = self.__DICT_for_COMPOSITION[config['junction_solver']]()
        self.NUM_USER_CLASSES = len(config['users'])

        # optional parameters
        if config.get('chosen_dt'):
            self.CHOSEN_DT = config['chosen_dt']

        # utility
        self.show = show_obj_attributes.__get__(self,self.__class__)


    def __set_veh_fd_parameters(self):
        """
        Select for all vehicle types the set of parameters compliant with the chosen
        fundamental diagram
        """

        fd_type = self.FUNDAMENTAL_DIAGRAM.__class__.__name__.lower()
        veh_types.Vehicle.FD_SHAPE = fd_type

    def __set_user_classes(self, config: dict, network: nx.MultiDiGraph):
        """
        Create USERS dictionary

        Output:
            * create users dictionary
            * set NUM_USER_CLASSES
            * user demand
                        
            Set user demand
                * convert string keys into number keys

            --> User demand is loaded as a dictionary

            user_demand = {
                    moment: value
                }

                moment: fraction of simulation horizon
                value: non negative value

                exp.

                * user_demand = {                       100 __ __ __
                    "0":100,      --> step function                 |
                    "0.5": 0                               0        |__ __ __
                                                                0.5
                    
                }

                * user_demand = {
                    "0": 100,                           200             __ __ __ __
                    "0.25": 0,                          100 __ __      |
                    "0.5":200,                                   |     |                               
                }                                       0        |__ __| 
                                                              0.25   0.5

            --> The demand is then "stretched" in order to cover all the time-step mantaining the "shape". 
            --> If DT is large enogh to include two consecutive values, a notification is printed to the user and the weighted average between the two is taken.
            --> Demand is pushed into the buffers at each time step depending on the the actual value of the step function
            --> Expressing the demand as a % of the simulation horizon allows to define it before knowing the number of time step for the simulation.

            * paths:

                Each path is stored as a Path() obj into the list paths which is returned by this function and used in __set_network() as a Network obj attribute. 
                Class-specific paths list simply point to the same objects. They ARE NOT copy!

                Memory                                             paths = [path1,path2,path3,path4]       user.paths = [path3,path4]
                ________________________________________                     |     |       |      |                          |    |
                |  * path1  - - - - - - - - - - - - - - | - - - - - - - - - -      |       |      |                          |    |
                |                   * path2 - - - - - - | - - - - - - - - - - - - -        |      |                          |    |
                |        * path3  - - - - - - - - - - - | - - - - - - - - - - - - - - - - - - - - | - - - - - - - - - - - - -     |
                |                                       |                                         |                               |
                |                * path4  - - - - - - - | - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
                |_______________________________________|
                
                Using aliasing a path object can be changed from paths or from some user.paths and automatically we are sure that this changes take effect everywere that object is used.
        """

        users_list = {k: value for k, value in config.items() if k == 'users'}['users']
        self.users = {}
        paths = []
        path_edge_lists = []
        paths_counter = 0

        for idx, user in enumerate(users_list):
            # numeric parameters
            index = idx
            origin = user['origin']
            destination = user['destination']
            behaviour_attributes = user['behaviour_attrs']

            # Check that origin != destination
            if origin == destination:
                attribute = [origin,destination]
                message = f'Class {idx} has same node {origin} set as origin and destination!'
                raise custom_exceptions.InconsistentData(attribute=attribute,message=message)

            # Vehicle
            vehicle = self.__DICT_for_COMPOSITION[user['veh_type']]()

            # Behaviour
            behaviour = self.__DICT_for_COMPOSITION[user['behaviour']](network,behaviour_attributes,origin,destination,index)

            # Paths --> paths
            class_paths_edges = behaviour.find_available_paths(network)

            # Find crossed nodes
            crossed_nodes_list = list()
            for path in class_paths_edges:
                no_keys_path = [(s, t) for s, t, k in path]  # --> get rid of the edge keys
                # -> extract all nodes from a edge path (nodes are redundant)
                crossed_nodes_list = crossed_nodes_list + list(itertools.chain(*no_keys_path))
            crossed_nodes_set = set(crossed_nodes_list)  # --> get rid of redundant nodes

            # Add class_paths to paths (if not existent) and creates objects
            if paths:  # if path is not empty
                #BUG: with two for loops it copies multiple times the same class_path
                new_paths = [class_path for class_path in class_paths_edges if class_path not in path_edge_lists]
            else:
                new_paths = class_paths_edges
            if new_paths:
                for path_edge_sequence in new_paths:
                    #NOTE: I don't like how path length is implemented.
                    path_length = np.array([network.edges[edge]['length'] for edge in path_edge_sequence]).sum()

                    path = Path(
                        idx=paths_counter,
                        edge_sequence=path_edge_sequence,
                        starting_node=path_edge_sequence[0][0],# source node of the first edge in the path edge sequence
                        ending_node=path_edge_sequence[-1][1],  # target node of the last edge in the paht edge sequence
                        travel_times=np.zeros([self.NUM_USER_CLASSES]),
                        length = path_length
                    )
                    paths.append(path)
                    path_edge_lists.append(path_edge_sequence)
                    paths_counter += 1

            # Class-specific paths point to the relative path in simulation.paths (aliasing)
            class_paths = [path for path in paths for path_edge in class_paths_edges if path.edge_sequence == path_edge]


            # matrices = [np.zeros([len(network.out_edges(node)) + 1, len(network.in_edges(node)) + 1]) for node in
            #             crossed_nodes_set]  # +1 for buffer and sink
            strategy = {node: None for node in crossed_nodes_set}

            # Add paths and strategy to behaviour obj
            behaviour.paths = class_paths
            behaviour.strategy = strategy

            # Set user demand
            demand = {float(key): value for key, value in user['demand'].items()}

            # Set user supply
            supply = {float(key): value for key, value in user['supply'].items()}

            # Create a user class
            user_class_obj = User(
                idx=index,
                origin=origin,
                destination=destination,
                demand=demand,
                supply=supply,
                vehicle=vehicle,
                behaviour=behaviour,
                strategy=strategy
            )

            # Insert user class into dictionary indexed with proper class index
            self.users[index] = user_class_obj

        self.__users_adaptive = {idx:user for idx,user in self.users.items() if user.behaviour.adaptive == True}
        self.__users_non_adaptive = {idx:user for idx,user in self.users.items() if user.behaviour.adaptive == False}

        return paths

    def __set_network(self, network, paths,config:dict):
        """
        * Check if dx is admissible and compute the maximum dt possible.
        * if self.CHOSEN_DT:
            compare CHOSEN_DT with max_allowed_dt
            if CHOSEN_DT < max_allowed_dt:
                DT = CHOSEN_DT
            else:
                DT = max_allowed_dt + warning

        Given the network obj:
            * check if dx <= than the shortest edge
            * compute max speed accroass all vehicle classes    
            * initialize network

        --> v_max is class-specific and doesn't depend on the edge attribute (more to come in v2)!
        """
        # Make a copy of the input network
        network = network.copy()  # --> a copy to avoid aliasing-related messes!

        # Add show
        network.show = show_obj_attributes.__get__(network,network.__class__)

        # Add paths list to the network obj
        network.paths = paths

        # Edges --> actual dx based on cfl 
        global_max_dt_allowed = np.inf
        for s, t, k, d in network.edges(keys=True, data=True):

            # edge space discretion
            num_cell_float = d['length'] / self.CHOSEN_DX
            num_cell_int = round(num_cell_float)
            if num_cell_int == 0:  # code robust in case chosen dx > min edge length
                print(f"dx smaller than edge length! --> dx adjusted for edge {s, t, k}")
                num_cell_int = 1
            dx = d['length'] / num_cell_int
            num_cells = num_cell_int

            # Core Tensors
            rho = np.zeros([self.NUM_USER_CLASSES, num_cells])
            total_rho = np.zeros([num_cells])
            class_distribution = np.zeros([self.NUM_USER_CLASSES, num_cells])
            q = np.zeros([self.NUM_USER_CLASSES, num_cells + 1])
            demand = np.zeros([self.NUM_USER_CLASSES, num_cells])
            supply = np.zeros([self.NUM_USER_CLASSES, num_cells])
            flow_in = np.zeros([self.NUM_USER_CLASSES])
            flow_out = np.zeros([self.NUM_USER_CLASSES])

            # Relevant infos <-- add here relevant quantities if needed
            cell_travel_times = np.zeros([self.NUM_USER_CLASSES, num_cells])
            travel_times = np.sum(cell_travel_times, axis = 1).reshape([self.NUM_USER_CLASSES,1])

            # # max allowed dt based on edge length and max speed
            # max_allowed_dt = dx / class_max_speed
            # if max_allowed_dt < global_max_dt_allowed:
            #     global_max_dt_allowed = max_allowed_dt

            # Create edge obj
            edge_obj = Edge(
                length=d['length'],
                priority=d['priority'],
                num_cells=num_cells,
                dx=dx,
                rho=rho,
                total_rho=total_rho,
                class_distribution=class_distribution,
                q=q,
                demand=demand,
                supply=supply,
                travel_times=travel_times,
                flow_in=flow_in,
                flow_out=flow_out
            )

            # Add proper fd related parameters <-- result of the coefficient combination
            for parameter in self.FUNDAMENTAL_DIAGRAM.parameters:
                # NOTE: defining fd parameters for each cell is useless for now --> future proof
                setattr(edge_obj, parameter,
                        np.zeros([self.NUM_USER_CLASSES, num_cells]))  # <-- fd parameters for each cell

            # clear edge attributes and save edge obj as new edge attribute
            d.clear()
            d['edge'] = edge_obj

        # Nodes
        for n, d in network.nodes(data=True):
            #node_idx = n
            in_edges = utils.sort_tuple([edge for edge in network.in_edges(n, keys=True)],2)  # 2nd position (source,target,_key_)
            out_edges = utils.sort_tuple([edge for edge in network.out_edges(n, keys=True)], 2)
            num_in_edges = len(in_edges)
            num_out_edges = len(out_edges)

            #NOTE:eventually refactor this in a separate funciton to facilitate updates
            # Set local keys for edges
            for source_local,edge in enumerate(out_edges):
                network.edges[edge]['edge'].source_local_key = source_local
            for target_local,edge in enumerate(in_edges):
                network.edges[edge]['edge'].target_local_key = target_local

            # Demands and supplies vectors
            demands = np.zeros([self.NUM_USER_CLASSES,num_in_edges + 1])
            supplies = np.zeros([self.NUM_USER_CLASSES,num_out_edges + 1])

            # rho/r from in edges and buffer
            class_distribution = np.zeros([self.NUM_USER_CLASSES, num_in_edges + 1])

            # Vehicles in the buffer
            veh_in_buffer = np.zeros([self.NUM_USER_CLASSES])

            # Vehicles in the sink
            veh_in_sink = np.zeros([self.NUM_USER_CLASSES])

            # Total vehicles in the buffer
            total_veh_in_buffer = 0

            # Total vehicles in the sink
            total_veh_in_sink = 0

            # Core Tensor --> distribution tensor
            distribution_tensor = np.zeros([self.NUM_USER_CLASSES, num_out_edges + 1, num_in_edges + 1])

            # Flow crossing the node
            flow_in = np.zeros([self.NUM_USER_CLASSES,num_in_edges])
            flow_out = np.zeros([self.NUM_USER_CLASSES,num_out_edges])
            
            # Flow coming from the buffer and goingo to the sink
            flow_from_buffer = np.zeros([self.NUM_USER_CLASSES])
            flow_to_sink = np.zeros([self.NUM_USER_CLASSES]) 

            # Priority vector
            priorities = [network.edges[edge]['edge'].priority for edge in in_edges]
            priorities.append(0) # demand priority (buffer priority) = 0 by default
            priorities = np.array(priorities)

            # Set demand priority (if present in config)
            if config['demand_priority'].get(str(n)):
                demand_priority = config['demand_priority'][str(n)]
                priorities = priorities * (1-demand_priority)
                priorities[-1] = demand_priority

            # Boundary conditions
            in_boundary_conditions = np.zeros([self.NUM_USER_CLASSES])
            out_boundary_conditions = np.zeros([self.NUM_USER_CLASSES])

            # Create node obj
            node_obj = Node(
                idx=n,
                in_edges=in_edges,
                out_edges=out_edges,
                num_in_edges=num_in_edges,
                num_out_edges=num_out_edges,
                flow_in=flow_in,
                flow_out=flow_out,
                flow_from_buffer=flow_from_buffer,
                flow_to_sink=flow_to_sink,
                priorities=priorities,
                distribution_tensor=distribution_tensor,
                demands=demands,
                supplies=supplies,
                class_distribution=class_distribution,
                veh_in_buffer=veh_in_buffer,
                total_veh_in_buffer=total_veh_in_buffer,
                veh_in_sink=veh_in_sink,
                total_veh_in_sink=total_veh_in_sink,
                in_boundary_conditions=in_boundary_conditions,
                out_boundary_conditions=out_boundary_conditions,
                is_origin=False,  # not an origin by default
                is_destination=False  # not a destination by default
            )

            d['node'] = node_obj

        # Composition with network. 
        self.network = network  # --> network stored as a simulation attribute

    def __link_strategies_to_distribution_tensors(self):
        """
        Through aliasing, user strategy dicionaries point (aliasing) to the right slices of distribution tensors
        in the nodes. This allows to automatically update the distribution tensors while updating the strategies.
        """
        # User strategy is linked, through alising, to the correct slice of distribution tensors 
        for idx,user in self.users.items():
            strategy = {node:self.network.nodes[node]['node'].distribution_tensor[idx,:,:] for node in user.strategy}
            user.behaviour.strategy.update(strategy)

    def __project_user_demand_supply_over_time(self):
        """
        * (stretch phase) Takes user demand expressed as a step-function in the continuos time domain and project it over the
          the discrete time domaina (given sim.NUM_INSTANTS)

          demand step-function --> vector: size[NUM_INSTANTS]
        * (compacting phase) compact the vectors of each user class into a tensor size:[NUM_CLASSES, NUM_INSTANTS]
        * (loading phase) save each tensor as a node attribute
        """
        for idx, user in self.users.items():
            #TODO: insert check data type for demand and supply

            # Demand
            sim_horizion_percentages = np.array([key for key in user.demand])  # --> convert sim_horizon % --> time step
            time_steps = np.rint(sim_horizion_percentages / self.SIM_HORIZON * self.NUM_INSTANTS).astype(int)

            time_step_set = set(time_steps)  # --> notify user if two time_steps are identical (demand information loss)
            if not (len(time_step_set) == len(time_steps)):  # -> means that at least one time step is redundant
                warnings.warn(
                    f"For demand of class {idx} two or more demand value are linked to the same time-step. Demand Info Loss!")
            
            demand = np.zeros([self.NUM_INSTANTS])  # --> create class-spcific demand vector
            for percentage, time_step in zip(sim_horizion_percentages, time_steps):
                demand[time_step:] = user.demand[percentage]

            self.network.nodes[user.origin]['node'].in_boundary_conditions[idx,:] = demand  # --> load demand into the proper class specific origin node
            if self.network.nodes[user.origin]['node'].is_origin == False:
                self.network.nodes[user.origin]['node'].is_origin = True  # --> flag the node as an origin node to speed up junction solver pre-processing

            # Supply
            sim_horizion_percentages = np.array([key for key in user.supply])  # --> convert sim_horizon % --> time step
            time_steps = np.rint(sim_horizion_percentages / self.SIM_HORIZON * self.NUM_INSTANTS).astype(int)

            time_step_set = set(time_steps)  # --> notify user if two time_steps are identical (supply information loss)
            if not (len(time_step_set) == len(time_steps)):  # -> means that at least one time step is redundant
                warnings.warn(
                    f"For supply of class {idx} two or more supply value are linked to the same time-step. Supply Info Loss!")

            supply = np.zeros([self.NUM_INSTANTS])  # --> create class-specific supply vector
            for percentage, time_step in zip(sim_horizion_percentages, time_steps):
                if user.supply[percentage] == 'inf':
                    supply[time_step:] = np.inf
                else:
                    supply[time_step:] = user.supply[percentage]

            # Save demand and supplies in the proper node
            self.network.nodes[user.destination]['node'].out_boundary_conditions[idx,:] = supply  # --> load supply into the proper class specific destination node
            if self.network.nodes[user.destination]['node'].is_destination == False:
                self.network.nodes[user.destination]['node'].is_destination = True
  
    def __mod_on_the_fly(self):
        """
        Allowd to modify several parameters of the simulation witout requiring a re_initialization!
        The controller acts here!
        --- Work in Progress ---
        """
        pass

    def __cfl(self):
        """
        Check cfl conditions and provides time discretization --> NUM_INSTANTS for the simulation
        """

        max_dQ = 0
        min_dx = np.inf
        for s, t, k, edge in self.network.edges(keys=True, data='edge'):
            # Combine fd coefficients
            self.FUNDAMENTAL_DIAGRAM.combine_coefficients(edge_obj=edge, users=self.users)

            # Find maximum speed accroass all vehicles type
            dQ = self.FUNDAMENTAL_DIAGRAM.max_dQ(edge)

            # Update maximum speed (max_dQ) and minimum dx (min_dx)
            max_dQ = max(dQ, max_dQ)
            min_dx = min(edge.dx, min_dx)

        # Max possibile dt
        max_allowed_dt = min_dx / max_dQ

        try:
            temp_dt = min(max_allowed_dt,self.CHOSEN_DT)
            if self.CHOSEN_DT>temp_dt:
                message = f'The chosen_dt violates CFL conditions since it is too large. DT will be set to the largest possible value.'
                warnings.warn(message)
        except:
            temp_dt = max_allowed_dt

        num_instants_float = self.SIM_HORIZON / temp_dt
        num_instants_int = np.ceil(num_instants_float).astype(int)
        self.NUM_INSTANTS = num_instants_int
        self.DT = self.SIM_HORIZON/self.NUM_INSTANTS


    def __time_discretization(self):
        """
        Knowing self.NUM_INSTANTS
        """
        # Edges
        for s, t, k, edge in self.network.edges(data='edge', keys=True):
            # Class-specific densities
            extension_array = np.zeros(
                [self.NUM_USER_CLASSES, edge.num_cells, self.NUM_INSTANTS - 1])  # -1 because rho0 is yet stored
            edge.rho = np.expand_dims(edge.rho, 2)
            edge.rho = np.concatenate([edge.rho, extension_array], 2)

            # Total densities
            extension_array = np.zeros([edge.num_cells, self.NUM_INSTANTS - 1])  # -1 becuse total_rho0 is yes stored
            edge.total_rho = np.expand_dims(edge.total_rho, 1)
            edge.total_rho = np.concatenate([edge.total_rho, extension_array], 1)


        # Nodes
        for idx, node in self.network.nodes(data='node'):
            
            # User Demand from Buffer
            extension_array = np.zeros([self.NUM_USER_CLASSES, self.NUM_INSTANTS - 1])
            node.in_boundary_conditions = np.expand_dims(node.in_boundary_conditions, 1)
            node.in_boundary_conditions = np.concatenate([node.in_boundary_conditions, extension_array], 1)

            # User Supply from Sink
            node.out_boundary_conditions = np.expand_dims(node.out_boundary_conditions, 1)
            node.out_boundary_conditions = np.concatenate([node.out_boundary_conditions, extension_array], 1)

    def __load_initial_conditions(self):
        """
        Load the initial densities on the edges.
        Now it doesn't do nothing --> rho0 = 0 for every edge!
        --- Work in Progress ---

            * Update total rho -> r
            * Update class-specific rho distributions
        """
        #TODO: Add update total rho and distributions when rho0 != 0
        pass

    def __update_path_behavioural_determinants(self,time_step):
        """
        Collect behavioural quantities into paths (aliasing --> auto update in sim.users[].paths)
        """
        for path in self.network.paths:
            path.travel_times = np.concatenate(
                [self.network.edges[edge]['edge'].travel_times for edge in path.edge_sequence],axis = 1).sum(axis = 1)

    def __compute_route_choices(self, time_instant):
        """
        Compute the strategy for each user class given the user behaviour.
        behavioural_determinants --> user.behaviour --> strategy (a.k.a dictionary of distribution matrices)

            * collect behavioural quantities into paths because "behaviours" are "routing problems" so there
              must be some path related quantity to be used into the behaviour.
        """

        if time_instant == 0:
            user_set = self.users
        else: 
            user_set = self.__users_adaptive

        # Loop over all users and invoke user.behaviours
        for idx,user in user_set.items():
            user.behaviour.compute_behaviour()

    def __solve_junctions(self,time_instant):
        """
        Solve junctions determining the throughput.
            * converts routing choices --> turning ratios
            * collects demand and supplies from all entries and exits
            * trigger Riemann Solver
            * scale the potential crossing flow (q_in) based on the class distribution -> flow_in
            * distribute flow_in over the downstream -> flow_out based on the distribution tensor
        """
        for _,node in self.network.nodes(data = 'node'):

            # Converts routing choices --> turning ratios
            self.__converts_routing_choices_to_turning_ratios(node,time_instant)
            
            # Manage buffer (if origin for at least one class)
            if node.is_origin:
                # Load buffer
                node.veh_in_buffer[:] += \
                    node.in_boundary_conditions[:,time_instant] * self.DT
                # Update total veh in the buffer 
                node.total_veh_in_buffer = node.veh_in_buffer[:].sum()
                # Collects demand from the buffer
                node.demands[:,-1] = node.veh_in_buffer[:] * 1 / self.DT
                # Collects rho/r from buffer
                node.class_distribution[:,-1] = np.divide(node.veh_in_buffer[:],node.total_veh_in_buffer,np.zeros_like(node.veh_in_buffer), where=node.total_veh_in_buffer != 0)
            
            # Manage sink (if destination for at least one class)
            if node.is_destination:
                node.supplies[:,-1] = node.out_boundary_conditions[:,time_instant]
                
            if node.in_edges:
                # Collects demands from in edges
                node.demands[:,:node.num_in_edges] = \
                    np.array([self.network.edges[edge]['edge'].demand[:,-1] for edge in node.in_edges]).T
                # Collects rho/r from in edges
                node.class_distribution[:,:-1] = \
                    np.array([self.network.edges[edge]['edge'].class_distribution[:,-1] for edge in node.in_edges]).T

            if node.out_edges:
                # Collects supplies from out_edges
                node.supplies[:,:node.num_out_edges] = \
                    np.array([self.network.edges[edge]['edge'].supply[:,0] for edge in node.out_edges]).T

            # Solve junction
            q_in = self.JUNCTION_SOLVER.solve(node.distribution_tensor,node.demands,node.supplies,node.priorities)

            # Actual flows (scaled)
            flow_in = q_in * node.class_distribution
            flow_out = np.einsum('cji,ci->cj',node.distribution_tensor,flow_in)

            # Save flow crossing the node from and to the network
            node.flow_in = flow_in[:,:-1]
            node.flow_out = flow_out[:,:-1]
            node.flow_from_buffer = flow_in[:,-1]
            node.flow_to_sink = flow_out[:,-1]

            # Update buffer
            if node.is_origin:
                # Drain buffer
                node.veh_in_buffer[:] -= node.flow_from_buffer*self.DT
                # Update total veh in the buffer 
                node.total_veh_in_buffer = node.veh_in_buffer.sum()
                # Update veh distribution in buffer
                with np.errstate(divide='ignore', invalid='ignore'):
                    node.class_distribution[:,-1] = np.nan_to_num(node.veh_in_buffer[:] / node.total_veh_in_buffer, nan=0)
            
            # Updates sink
            if node.is_destination:
                # Fill sink
                node.veh_in_sink = node.flow_to_sink.sum()

            # Update behavioural determinants on nodes (nothing for now)

    def __converts_routing_choices_to_turning_ratios(self,node,time_instant):
        if time_instant == 0:
            user_set = self.users
        else:
            user_set = self.__users_adaptive

        for _,user in user_set.items():
            if node.idx in user.behaviour.strategy:
                if node.out_edges:
                    if node.idx == user.origin:
                        # this peace of code is redundant but inserted to get more "human readable" distribution tensors.
                        # superflous from an implementation point of view.
                        a = []
                        paths = user.behaviour.paths
                        route_choice_probabilities = user.behaviour.p
                        idx_den = [idx for idx,path in enumerate(paths) if any(edge in path.edge_sequence for edge in node.out_edges)] #<- indexes of all paths crossing one of the out_edges
                        sum_den = user.behaviour.p[idx_den].sum()
                        for edge_idx,edge in enumerate(node.out_edges):
                            idx_num = [idx for idx,path in enumerate(paths) if edge in path.edge_sequence] #<- index of the path crossing edge
                            sum_num = user.behaviour.p[idx_num].sum()
                            a.append(sum_num/sum_den)
                        a = np.array(a) # <-- no reshape to avoid broadcasting errors
                        node.distribution_tensor[user.idx,:-1,-1] = a
                        
                    else:
                        a = []
                        paths = user.behaviour.paths
                        route_choice_probabilities = user.behaviour.p
                        idx_den = [idx for idx,path in enumerate(paths) if any(edge in path.edge_sequence for edge in node.out_edges)] #<- indexes of all paths crossing one of the out_edges
                        sum_den = user.behaviour.p[idx_den].sum()
                        for edge_idx,edge in enumerate(node.out_edges):
                            idx_num = [idx for idx,path in enumerate(paths) if edge in path.edge_sequence] #<- index of the path crossing edge
                            sum_num = user.behaviour.p[idx_num].sum()
                            a.append(sum_num/sum_den)
                        a = np.array(a).reshape([len(a),1])
                        #idx_in_edges = [idx for idx,edge in enumerate(node.in_edges) if any(edge in path.edge_sequence for path in paths)]
                        node.distribution_tensor[user.idx,:-1,:] = a
    
                else:
                    if node.idx == user.destination: #<-- superfluous because if node_idx in user stragey and node.out_edges == 0 necessarily node is user destination
                        paths = user.behaviour.paths
                        idx_in_edges = [idx for idx,edge in enumerate(node.in_edges) if any(edge in path.edge_sequence for path in paths)]
                        node.distribution_tensor[user.idx,-1,idx_in_edges] = 1
            else: #<-- user class doesn't cross the node. No action needed 
                pass
            
    def __solve_edges(self, time_instant):
        """
        Compute edge dynamics
            * collects flow_in and flow_out from the upstream and downstram nodes
            * trigger the godunov scheme (update rhos)
            * update other quantities
                * update total rho
                * demand
                * supply
        """

        for s,t,k, edge in self.network.edges(keys=True,data='edge'):
            # Get in flow from source node
            edge.flow_in = self.network.nodes[s]['node'].flow_out[:,edge.source_local_key]
            edge.flow_out = self.network.nodes[t]['node'].flow_in[:,edge.target_local_key]

            # Transfer boundary flows into flow matrix (yes scaled)
            edge.q[:,0] = edge.flow_in
            edge.q[:,-1] = edge.flow_out

            # Compute internal flow transfers (min[demand,supply])
            edge.q[:,1:-1] = edge.class_distribution[:,:-1] * np.minimum(edge.demand[:,:-1],edge.supply[:,1:])

            # Update rho
            edge.rho[:,:,time_instant + 1]  = edge.rho[:,:,time_instant] - self.DT/edge.dx * (edge.q[:,1:] - edge.q[:,:-1])

            # Update total rho
            edge.total_rho[:,time_instant + 1] = edge.rho[:,:,time_instant + 1].sum(axis=0)

            # Update class distributions
            #NOTE: np.divide with where is 3 times faster than np.nan_to_num on average but 4 times slower than 
            # edge.rho/np.minimum(edge.total_rho, m) where m -> very small number like 0.00000000001
            edge.class_distribution[:,:] = np.divide(edge.rho[:,:,time_instant + 1],edge.total_rho[:,time_instant + 1], 
            np.zeros_like(edge.rho[:,:,time_instant+1]), where=edge.total_rho[:,time_instant+1] !=0)

            # Compute demand and supply for the next instant
            edge.demand[:,:], edge.supply[:,:] = self.FUNDAMENTAL_DIAGRAM.get_edge_demands_supplies(edge,time_instant + 1)

            # Update behavioural determinants on edges
            self.__update_edge_behavioural_determinants(edge=edge, time_step=time_instant)

    def __init_edge_behavioural_determinants(self):
        """
        Update those quantities (usually edge-related) that influence some routing behavioural rule.

        Added quantities:
            * travel time

        Remember: 
            in case a new behavioural determinant is added here, the same quantity must be added (as a np.zeros) in
            edge init and path init.
        """

        # Update edge travel times
        for s,t,k,edge in self.network.edges(data='edge', keys=True):
            # update edge travel time
            self.__update_edge_behavioural_determinants(edge,0)

    def __update_edge_behavioural_determinants(self,edge,time_step):
        """
        Update all edge-related behavioural determinants quantities (such as class-specific edge travel time)
        """
        # update edge class-specific travel time
        self.__update_edge_travel_times(edge,time_step)

    def __update_edge_travel_times(self,edge,time_step):
        """
        Update travel time of an edge
        """

        class_cell_specific_speeds = self.FUNDAMENTAL_DIAGRAM.get_V(edge,time_step)
        #edge.class_cell_specific_speeds = class_cell_specific_speeds

        #NOTE: handle cases when v=0 --> travel_times = inf --> potential problems in behaviours
        # This problem is faced directly in the behaviours
        
        with np.errstate(divide = 'ignore', invalid='ignore'):
            class_cell_specific_travel_times = edge.dx / class_cell_specific_speeds
        
        edge.cell_travel_times = class_cell_specific_travel_times # sum cell_specific travel times --> travel time along the edge
        edge.travel_times = np.sum(class_cell_specific_travel_times, axis= 1).reshape([self.NUM_USER_CLASSES,1])

    def __init_demands_supplies(self):
       
       for s,t,k,edge in self.network.edges(keys=True,data='edge'):
        demand, supply = self.FUNDAMENTAL_DIAGRAM.get_edge_demands_supplies(edge,0) #<-- use rho0
        self.network.edges[s,t,k]['edge'].demand[:,:] = demand
        self.network.edges[s,t,k]['edge'].supply[:,:] = supply