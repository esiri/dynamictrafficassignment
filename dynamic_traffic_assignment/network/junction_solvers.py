import numpy as np
import warnings

class RiemannSolver():
    """
    A class for junction solvers.
    A junction solver takes as input: 
        * demand vector (or matrix for multiclass)
        * suppy vector (or matric for multiclass)
        * distribution matrix (or tensor for multiclass)
        * priority vector
    and returns:
        *Q_in = {q_ic} vector (or matrix for multiclass) where
         q_ic = flow of class c entering the node from edge i.
    """

    def __init__(self,type):
        self.type = type

class Priority(RiemannSolver):
    """
    Numpy vectorized implementation of the Priority Riemann Solver presented in:

        Maria Laura Delle Monache, Paola Goatin, Benedetto Piccoli. Priority-based Riemann solver for
        traffic flow on networks. Communications in Mathematical Sciences, 2018, 16 (1), pp.185-211.
        10.4310/CMS.2018.v16.n1.a9. hal-01336823

        https://inria.hal.science/hal-01336823

        adapted for Multi Class Scenario
    """

    def __init__(self):
        type = 'priority riemann solver'
        super().__init__(type)

    def solve(self,A,D,S,P):
        

        a_exit_index = A.ndim - 2
        d_entry_index = D.ndim - 1
        s_exit_index = S.ndim - 1
        num_class = D.shape[0] # demand and supply have always 2 dimension, even in single class case

        # Set of explored entries
        J = np.zeros(D.shape) # J[c,i] = 0 if entry i is "unexplored" for class c, Jc[c,i] = 1 if explored --> means the Q_in[c,i] has been fixed yet.
        Q_in = np.zeros(D.shape)

        # Fix Q_in[c,i] if D[c,i] = 0
        test = (D == 0) # True if D[c,i] == 0, False otherwise
        J[test] = 1 # now Q_in[c,i]: D[c,i] == 0 are fixed to zero (Q_in = 0 by default, no need to change)

        # Fix Q_in[c,i] if A[c,:,i] == 0 (all a_c:i = 0 on column i for class c)
        test = (A.sum(axis=a_exit_index)==0)
        J[test] = 1

        while (1-J).sum() > 0:

            with np.errstate(divide='ignore', invalid='ignore'): #<-- allows division by 0
                H_in = D/(P*(1-J)) # (1-Jc)[c,i] = 0 if entry i is "explored" for class c; 1 if "unexplored"
                H_out = (S - np.einsum('cji,ci->cj',A,J*Q_in))/np.einsum('cji,ci->cj',A, (1-J)*P)
            
            with warnings.catch_warnings():
                warnings.simplefilter("ignore", category=RuntimeWarning)
                H_in_min = np.nanmin(H_in, axis=d_entry_index).reshape([num_class,1])
                where_H_in_mins = ((H_in == H_in_min) & (np.isfinite(H_in)))
                
                H_out_min = np.nanmin(H_out, axis=s_exit_index).reshape([num_class,1])
            
                H = np.hstack([H_out_min,H_in_min])
                H_min = np.nanmin(H, axis=a_exit_index).reshape([num_class,1])
                H_min[~np.isfinite(H_min)] = 0 
            
            # Define I set (set where we'll fix Q_in)
            I = np.where(H_out_min <= H_in_min, (1-J[:]), (1-J[:])*where_H_in_mins[:])
    
            # Compute Q_in
            Q_in += H_min*I*P

            # Update J (set of explored entries)
            J += I
        
        return Q_in


class SoftPriority(RiemannSolver):
    """
    Numpy vectorized implementation of the Soft Priority Riemann Solver presented in:

        Maria Laura Delle Monache, Paola Goatin, Benedetto Piccoli. Priority-based Riemann solver for
        traffic flow on networks. Communications in Mathematical Sciences, 2018, 16 (1), pp.185-211.
        10.4310/CMS.2018.v16.n1.a9. hal-01336823

        https://inria.hal.science/hal-01336823

        adapted for Multi Class Scenario
    """
    def __init__(self):
        type = 'soft priority riemann solver'
        super().__init__(type)

    def solve(self,A,D,S,P):

        a_exit_index = A.ndim - 2
        d_entry_index = D.ndim - 1
        s_exit_index = S.ndim - 1
        num_class  = D.shape[0]


        # Set of explored entries
        J = np.zeros(D.shape)
        Q_in = np.zeros(D.shape)

        # Fiz Q_in[c,i] if D[c,i] = 0
        test = (D==0)
        J[test] = 1

        # Fix Q_in[c,i] if A[c,:,i] = 0 
        test = (A.sum(axis=a_exit_index)==0)
        J[test] = 1

        while (1-J).sum() > 0:
            A_test = np.ceil(A)
            with np.errstate(divide='ignore', invalid='ignore'): #<-- allows division by 0
                H_in = D/(P*(1-J)) # (1-Jc)[c,i] = 0 if entry i is "explored" for class c; 1 if "unexplored"
                H_out = (S - np.einsum('cji,ci->cj',A,J*Q_in))/np.einsum('cji,ci->cj',A, (1-J)*P)
                
            with warnings.catch_warnings():
                warnings.simplefilter("ignore", category=RuntimeWarning)
                H_in_min = np.nanmin(H_in, axis=d_entry_index).reshape([num_class,1])
                where_H_in_mins = ((H_in == H_in_min) & (np.isfinite(H_in)))

                H_out_min = np.nanmin(H_out, axis=s_exit_index).reshape([num_class,1])
                where_H_out_mins = ((H_out == H_out_min) & (np.isfinite(H_out))).astype(bool)
                where_H_out_not_mins = ~ where_H_out_mins

                H = np.hstack([H_out_min,H_in_min])
                H_min = np.nanmin(H, axis=a_exit_index).reshape([num_class,1])
                H_min[~np.isfinite(H_min)] = 0

            # Select columns where aji != 0, for all j: h_j = H_out_min
            A_test[where_H_out_not_mins,:] = 0
            a_ji_diff_zero = (A_test.sum(axis=1)>0)

            # Define I set
            I = np.where(H_out_min <= H_in_min, (1-J[:])*a_ji_diff_zero[:], (1-J[:])*where_H_in_mins[:])
            
            # Compute Q_in
            Q_in += H_min*I*P
            J += I

        return Q_in