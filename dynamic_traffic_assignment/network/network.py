import networkx as nx
from dynamic_traffic_assignment.utils.utils import show_obj_attributes


class Edge:
    """
    Traffic network edge class.
    """

    def __init__(self, **attrs: dict):
        for attr, value in attrs.items():
            setattr(self, attr, value)

        self.show = show_obj_attributes.__get__(self, self.__class__)


class Node:
    """
    Traffic network junction class
    """

    def __init__(self, **attrs):
        for attr, value in attrs.items():
            setattr(self, attr, value)

        self.show = show_obj_attributes.__get__(self, self.__class__)


class Path:
    """
    A class representing an existing path in a MultiDiGraph network.
    """

    def __init__(self, **attrs):
        """
        The init process is managed by a simulation object.
        """
        for attr, value in attrs.items():
            setattr(self, attr, value)

        self.show = show_obj_attributes.__get__(self, self.__class__)