import numpy as np
import networkx as nx
from dynamic_traffic_assignment.utils.utils import show_obj_attributes


class Behaviour:
    """
    User routing behaviour
    """

    def __init__(self, type, adaptive):
      self.type = type
      self.adaptive=adaptive
      self.__set = False

      # available paths
      self.paths = None


        
class Logit_shortest(Behaviour):
    """
    Routing choice follows a logit distribution. 


      * (1) Path choice probability: pi = e^(-a * tt_i) / sum_k(e^(-a * tt_k)) 
                                  = 1 / (1 + sum_(k!=i) e^(-a*tt_k))  <-- formula actually implemented
      * (2) Smotthing: p_new = w*p_target + (1-w)*p_old

      * MANDATORY PARAMETERS in JSON: 

        "behaviour_attrs": {
          "sensitivity": 'a' value in (1),
          "responsivity": 'w' value in (2)
        }    

      * while computing pi the i-th element of sum_k e^(-a*tt_k) is masked and is not used 
        in the sum.

      The multivariate logit distribution is implemented in 2 different ways:
          
          * loop --> classical python loop. One mask vector is created for each loop, for example:
            
            exps = [exp0,exp1,exp2,exp3]
            mask_array = [False,False,False,False]
            
            Then whitin the loop --> mask_array[idx] = True
            So, for example, for idx = 1 we get --> masked_array = [False, True, False, False]
            When we use numpy.ma.masked_array(exps,mask = mask_array) we get --> exps_masked = [exp0, --, exp2, exp3].
            When we do an operation on the array the 1st value is ignored. This allows to implement sum_(k!=idx) e^(-a*tt_k)
            Thus, we can easily compute --> p[idx] = 1 / [1 + sum_(k!=idx) e^(-a*tt_k)]

          * vector --> based on the same idea, the "masking" process is obtain vectorially. To do that we expand the
            
            previous vectors into matrices:
            
            exps_matrix = [[exp0,exp1,exp2,exp3],
                          [exp0,exp1,exp2,exp3],
                          [exp0,exp1,exp2,exp3],
                          [exp0,exp1,exp2,exp3]]
          mask_matrix = [[True, False, False, False],
                          [False, True, False, False],
                          [False, False, True, False],
                          [False, False, False, True]]
          exps_masked = [[--,exp1,exp2,exp3],
                          [exp0,--,exp2,exp3],
                          [exp0,exp1,--,exp3],
                          [exp0,exp1,exp2,--]]

          if we compute p[:] = 1 / (1 + exps_masked.sum(axis = 1)) (<-- sum over the column)
          we get p[idx] = 1 / [1 + sum_(k!=idx) e^(-a*tt_k)].

      !!! Both formulations are given because the way their performance 
          varies according to the scale of the problem is not trivial.
          Indicatevely for a small number of paths 'loop' performs better. Around 50 paths,
          vector wins, but if the number of paths is even bigger loop wins again. 

          --> USE BACKEND TO USE ONE OVER THE OTHER
      #NOTE: elaborate on this
      To avoid any problem related to having p[i] too small:
        * path_travel_times --> represents the powers of the negative exponentials. They cannot be to bit, otherwise
        the exp --> 0 and is cutted off due to underflow. 
    """

    def __init__(self,network, behaviour_attributes, user_origin,user_destination,user_idx):
        super().__init__(type="logit_shortest_paths",adaptive=True)

        self.__origin = user_origin
        self.__destination = user_destination
        self.__idx = user_idx

        # default attributes
        self.sensitivity = 1
        self.responsivity = 0.2
        self.cutoff = None
        self.p = None

        # add show
        self.show = show_obj_attributes.__get__(self, self.__class__)
        
        # add attributes (if presents)
        if behaviour_attributes.get('sensitivity'):
          self.sensitivity = behaviour_attributes['sensitivity']
        if behaviour_attributes.get('responsivity'):
          self.responsivity = behaviour_attributes['responsivity']
        if behaviour_attributes.get('cutoff'):
          self.behaviour_attributes['cutoff']

        self.strategy = None
        self.paths = None
        
    def __compute_path_choice_probabilities(self):
      
      #coefficients
      backend = 'loop' #--> {'loop','vector'}
      cut_off = 100 # --> user became insensible arount this travel time

      # Unrolling travel times
      path_travel_times = np.array([path.travel_times[self.__idx] for path in self.paths])

      # Meaningful travel times (apply cutoff)
      with np.errstate(divide = 'ignore', invalid='ignore'): 
          relevant_path_travel_times = (-np.exp(-(np.divide(1,cut_off))*path_travel_times)+1)*cut_off

      relative_path_travel_times = relevant_path_travel_times/relevant_path_travel_times.min() #--> relative to the minimum
      
      sensitivity = np.min([self.sensitivity, 700]) # --> avoid underflow

      exps =np.exp(-sensitivity*relative_path_travel_times)

      p = np.zeros(exps.size) #<-- pi = fraction of users choosing path i-th

      if backend == 'loop':
        for idx,exp in enumerate(exps):
          mask = np.ones_like(exps)
          mask[idx] = 0
          p[idx] = 1/(1 + exps[idx]**(-1) * (exps*mask).sum())

      elif backend == 'vector':
        exps_matrix = np.tile(exps,(exps.size,1))
        mask = (~ np.diagflat(np.ones_like(exps,dtype=bool))).astype(int)
        p[:] = 1/(1 + exps[:]**(-1) * (exps_matrix * mask).sum(axis=1))

      # Rounding of p elements
      return p.round(decimals=6)

    def __smoothing(self, p_target):
      """
      Apply smoothing to the new computed path choice probabilities (p_target).
        * p_new = w*p_target + (1-w)*p_old

        * For time_step = 0 'self.responsivity * (p_target - self.p) + self.p' will fail because
          self.p = None. 
        * For this reason the except is triggered and p_new = p_old without applying any smoothing. 
      """ 

      try:
        p_new = self.responsivity * (p_target - self.p) + self.p
      except: #<-- triggered for k = 0 when sel.p = None
        p_new = p_target

      return  p_new

    def find_available_paths(self, network):
      """
      Find all availble paths
      """
      return  list(nx.all_simple_edge_paths(network, source=self.__origin, target=self.__destination,cutoff=self.cutoff))
    
    def compute_behaviour(self):
      """
      Compute route choice:
        * compute p_target vector <-- path choice probabilities
        * apply smoothing p_new = w * p_target + (1-w)p_old
      """
      p_target = self.__compute_path_choice_probabilities()
      self.p = self.__smoothing(p_target)

    def show_attrs(self):
      """Shoe behaviour attributes in one shot
      """
      # Bhaviour info
      beh_name = f'type: {self.type}'
      beh_adaptive = f'adaptive: {self.adaptive}'
      beh_paths = [path.edge_sequence for path in self.paths]
      beh_paths = f'available paths: {beh_paths}'
      beh_sensitivity = f'travel time sensitivity: {self.sensitivity}'
      beh_responsivity = f'responsivity: {self.responsivity}'

      # Output string
      return  f'Behaviour info:\n{beh_name}\n{beh_adaptive}\n{beh_paths}\n{beh_sensitivity}\n{beh_responsivity}'


class Fixed_paths(Behaviour):
  """
  Users move on a pre-selected set of paths with fixed distribution

  
  """

  def __init__(self,network,behaviour_attributes,user_origin,user_destination,user_idx):
    super().__init__(type='fixed_paths', adaptive=False)

    self.__origin = user_origin
    self.__destination = user_destination
    self.__idx = user_idx

    # add show
    self.show = show_obj_attributes.__get__(self,self.__class__)

    # add attributes
    self.__paths_list = []
    self.__distribution = []
    self.paths = None
    self.strategy = None

    self.__get_paths_list(behaviour_attributes['paths'])
    self.__get_paths_distribution(behaviour_attributes['distribution'])

  def __get_paths_distribution(self,distributions):
    """
    Save fixed choice probabilities 
    """
    self.__distribution = eval(distributions)

  def __get_paths_list(self,paths_list):
    """
    Save fixed path list
    """
    for path in paths_list:

      eval_path = eval(path)
      self.__paths_list.append(eval_path)

  def find_available_paths(self,network):
    """
    Find paths saved in behaviour_attrs
    """
    paths = self.__paths_list.copy()

    del self.__paths_list # redundant
    return paths

  def compute_behaviour(self):
      """
      Compute route choice:
        * set route choice probabilities based on behaviour_attrs distribution values
      """
      self.p = np.array(self.__distribution).astype(float)

  def show_attrs(self):
      """
      Show behaviour attributes in one shot
      """
      # Bhaviour info
      beh_name = f'type: {self.type}'
      beh_adaptive = f'adaptive: {self.adaptive}'
      beh_paths = [path.edge_sequence for path in self.paths]
      beh_paths = f'available paths: {beh_paths}'
      beh_distribution = f'distribution over paths: {self.__distribution}'

      # Output string
      return f'Behaviour info:\n{beh_name}\n{beh_adaptive}\n{beh_paths}\n{beh_distribution}'






