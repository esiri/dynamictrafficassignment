import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from IPython.display import HTML


# functions
def show_obj_attributes(obj):
    """
     Function to show object attributes. It is a shortcuts for .__dict__.keys() command.
     """

    print(list(obj.__dict__.keys()))


def del_multiple_keys(dictionary: dict, black_list: set):
    """
    Removes multiple key/value pairs from a dictionary
    """

    for key in black_list:
        dictionary.pop(key)


def sort_tuple(tup, element):
    """
    Sorts a list of tuples based on the element-th element
    """
    return sorted(tup, key=lambda x: x[element])


def is_in_range(number, list):
    if (number <= list[-1]) and (number >= list[0]):
        return True
    else:
        return False


def is_a_number(number):
    return (type(number) == int) or (type(number) == float)

def dynamic_density_plot(density,interval,mode = 'notebook',start=0,stop=-1):
    """
    Creates animated plots of a certain quantity
    """

    fig, ax = plt.subplots()
    num_instants = density.shape[1]
    time_steps = range(num_instants)
    time_steps = time_steps[start:stop]

    min_density = 0
    max_density = np.max(density)

    def update(frame):
        ax.clear()
        ax.set(ylim=[min_density,max_density + 10])
        ax.set_title(f'Time instant: {frame}')
        ax.stem(density[:,frame], linefmt='g-', markerfmt='go', basefmt='r-')

    anim = animation.FuncAnimation(fig=fig,func=update,frames=time_steps,interval=interval)

    if mode == 'notebook':
        plt.close()
        return anim
    elif mode == 'native':
        plt.show()
        
    def copy(simulation):
        """
        An utility to copy a simulation object into a fresh one.

            * the utility check w
        """